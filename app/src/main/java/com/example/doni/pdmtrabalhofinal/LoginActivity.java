package com.example.doni.pdmtrabalhofinal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private EditText etEmail = null;
    private EditText etPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Bundle params = getIntent().getExtras();

        if(params != null) {
            etEmail = (EditText) findViewById(R.id.email);
            etEmail.setText(params.getString("email"));
        }

        Button btn = (Button) findViewById(R.id.sign_in);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyLogin(v);
            }
        });
    }

    private void verifyLogin(View v) {
        if("eu@eu.com".equals(getEmail()) && "abc123".equals(getPassword())) {
            Intent data = new Intent();
            data.putExtra("email", getEmail());
            data.putExtra("token", "aaaaaaaaaaaaaaaaaaaaaaa");
            data.putExtra("remember", ((CheckBox) findViewById(R.id.remember_me)).isChecked());
            setResult(1, data);
            finish();
        } else {
            Snackbar.make(v, "e-mail ou senha incorretos", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }


    }

    private String getEmail() {
        if(etEmail == null)
            etEmail = (EditText) findViewById(R.id.email);

        return etEmail.getText().toString();
    }

    private String getPassword() {
        if(etPassword == null)
            etPassword = (EditText) findViewById(R.id.password);

        return etPassword.getText().toString();
    }

}
