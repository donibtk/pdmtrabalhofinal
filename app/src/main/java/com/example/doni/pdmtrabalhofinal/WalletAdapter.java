package com.example.doni.pdmtrabalhofinal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by doni on 03/06/17.
 */

public class WalletAdapter extends BaseAdapter {

    private WalletDB wdb;
    LayoutInflater inflater;

    public WalletAdapter(WalletDB wdb,
                         LayoutInflater inflater) {
        this.wdb = wdb;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return wdb.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Wallet w = wdb.get(position);
        View v = inflater.inflate(R.layout.wallet_item_list, null);
        ((TextView)v.findViewById(R.id.wallet_amount)).setText(w.getAmount() + "");
        ((TextView)v.findViewById(R.id.wallet_name)).setText(w.getName());
        return v;
    }

    @Override
    public long getItemId(int position) {
        return wdb.get(position).getId();
    }

    @Override
    public Object getItem(int position) {
        return wdb.get(position);
    }
}
