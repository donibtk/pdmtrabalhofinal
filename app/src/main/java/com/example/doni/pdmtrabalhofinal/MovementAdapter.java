package com.example.doni.pdmtrabalhofinal;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by doni on 08/06/17.
 */

public class MovementAdapter extends BaseAdapter {

    private MovementDB mdb;
    LayoutInflater inflater;
    private int id;

    public MovementAdapter(LayoutInflater inflater, int id) {
        this.inflater = inflater;
        this.id = id;
    }

    @Override
    public int getCount() {
        return mdb.getMovements(id).size();
    }

    @Override
    public Object getItem(int position) {
        return mdb.getMovements(id).get(position);
    }

    @Override
    public long getItemId(int position) {
        return mdb.getMovements(id).get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movement m = mdb.getMovements(id).get(position);
        View v = inflater.inflate(R.layout.movement_list_item, null);
        ((TextView) v.findViewById(R.id.movement_item_amount)).setText(m.getValue() + "");
        ((TextView) v.findViewById(R.id.movement_item_detail)).setText(m.getDescription());
        return v;
    }

    public void setMovementDB(MovementDB movementDB) {
        this.mdb = movementDB;
    }
}
