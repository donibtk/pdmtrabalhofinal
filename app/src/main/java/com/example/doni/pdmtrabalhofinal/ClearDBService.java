package com.example.doni.pdmtrabalhofinal;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

public class ClearDBService extends IntentService {

    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private final int N_ID = 876465247;

    public ClearDBService() {
        super("ClearDB Service");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        synchronized (this) {

            notify("Removendo registros orfãos", "Verificando...");

            try {
                wait(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if(WalletDB.removed.size() == 0) {
                notify("Concluido", "Nenhum registro excluido");
            } else {
                int removed = 0;
                MovementDB mdb = new MovementDB(this, null);
                for(int i = 0; i < WalletDB.removed.size(); i++) {
                    int wr = WalletDB.removed.get(i);
                    List<Movement> mv = mdb.getMovements(wr);
                    removed += mv.size();
                    for(int n = 0; n < mv.size(); n++) {
                        mdb.remove(mv.get(n));
                    }
                }

                if(removed > 0) {
                    notify("Concluido", removed + " registros orfãos removidos.");
                }
                else {
                    notify("Concluido", "Nenhum registro excluido");
                }
            }
        }
    }

    private void notify(String title, String content) {
        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(content);
        mBuilder.setSmallIcon(android.R.drawable.ic_dialog_alert);

        Intent result = new Intent(this, MainActivity.class);
        mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, result, 0));

        mBuilder.setAutoCancel(true);

        if(mNotificationManager == null)
            mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(N_ID, mBuilder.build());
    }
}
