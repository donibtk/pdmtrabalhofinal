package com.example.doni.pdmtrabalhofinal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditWalletActivity extends AppCompatActivity {

    private Integer id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_wallet);
        ((Button) findViewById(R.id.edit_wallet_save_button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        Bundle data = getIntent().getExtras();


        if(data == null || data.getInt("id", 0) == 0) return;

            id = data.getInt("id", 0);

            ((EditText)findViewById(R.id.edit_wallet_name))
                    .setText(data.getString("name"));
            ((EditText)findViewById(R.id.edit_wallet_amount))
                    .setText(data.getDouble("amount", 0) + "");

    }

    private void save() {
        Intent data = new Intent();
        data.putExtra("name", ((EditText)findViewById(R.id.edit_wallet_name)).getText().toString());
        double amount = Double.parseDouble(((EditText)findViewById(R.id.edit_wallet_amount)).getText().toString());
        data.putExtra("amount", amount);

        if(id == null) {
            setResult(MainActivity.NEW_WALLET_CODE, data);
        } else {
            data.putExtra("id", id.intValue());
            setResult(MainActivity.EDIT_WALLET_CODE, data);
        }

        finish();
    }
}
