package com.example.doni.pdmtrabalhofinal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ConfirmWalletDestroyDialog extends DialogFragment {

    private ConfirmWalletDestroyDialogListener listener;

    public ConfirmWalletDestroyDialog() {}

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("Confirma exclusão?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.onDestroyWalletClicked();
            }
        });

        builder.setNegativeButton("Não", null);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ConfirmWalletDestroyDialogListener) context;
    }

    public interface ConfirmWalletDestroyDialogListener {
        void onDestroyWalletClicked();
    }
}
