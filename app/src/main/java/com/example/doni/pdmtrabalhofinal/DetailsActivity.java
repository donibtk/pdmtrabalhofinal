package com.example.doni.pdmtrabalhofinal;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity implements MovementNewOrEditFragment.InsertAmountListener{

    private Integer id;
    private MovementAdapter movementAdapter;
    private MovementDB mdb;
    private Movement movement;
    private TextView tvAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Bundle params = getIntent().getExtras();

        if(params == null) {
            finish();
            return;
        }

        TextView label = (TextView) findViewById(R.id.detail_wallet_title);
        label.setText(params.getString("wallet_name"));
        tvAmount = (TextView) findViewById(R.id.detail_wallet_amount);
        tvAmount.setText(params.getDouble("wallet_amount") + "");
        id = params.getInt("wallet_id");
        movementAdapter = new MovementAdapter(this.getLayoutInflater(), id);
        mdb = new MovementDB(this, movementAdapter);
        movementAdapter.setMovementDB(mdb);

        ListView lv = (ListView) findViewById(R.id.detail_wallet_list_view);
        lv.setAdapter(movementAdapter);

        registerForContextMenu(lv);

        Button btn = (Button) findViewById(R.id.detail_wallet_button);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMovement();
            }
        });

    }

    public void addMovement() {
        FragmentManager fm = getSupportFragmentManager();
        MovementNewOrEditFragment m = new MovementNewOrEditFragment();
        movement = null;
        m.show(fm, "Novo");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.movement_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.movement_menu_edit:
                FragmentManager fm = getSupportFragmentManager();
                MovementNewOrEditFragment m = new MovementNewOrEditFragment();
                AdapterView.AdapterContextMenuInfo editpos = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                movement = mdb.getMovements(id).get(editpos.position);
                m.show(fm, "Novo");
                return true;

            case R.id.movement_menu_delete:
                AdapterView.AdapterContextMenuInfo removepos = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                movement = mdb.getMovements(id).get(removepos.position);
                mdb.remove(movement);
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDialogPositiveClick(Double amount, String description) {
        Toast.makeText(this, "Valor: " + amount, Toast.LENGTH_SHORT).show();
        if(movement == null) {
            movement = new Movement();
            movement.setDescription(description).setValue(amount).setWalletId(id);
            mdb.add(movement);
        }
        else {
            movement.setDescription(description).setValue(amount);
            mdb.update(movement);
        }

    }

    public Movement getMovement() {
        return movement;
    }
}
