package com.example.doni.pdmtrabalhofinal;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity
        implements PopupMenu.OnMenuItemClickListener,
                    ConfirmWalletDestroyDialog.ConfirmWalletDestroyDialogListener {

    private SharedPreferences config;
    private WalletDB wdb;
    protected static WalletAdapter walletAdapter;
    private ListView listView;
    private static int walletToDestroy;

    public static final int LOGIN_CODE = 1;
    public static final int NEW_WALLET_CODE = 2;
    public static final int EDIT_WALLET_CODE = 3;
    public static final int DETAIL_CODE = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        config = getSharedPreferences("config", MODE_PRIVATE);

        ConfigStorage.loadData(this);
        if(!ConfigStorage.isLoggedIn()) {
            doLogin();
        } else {
            loadWallets();
        }

        Button add = (Button) findViewById(R.id.button_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showWalletMenu(v);
            }
        });

    }

    private void showWalletMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.wallet_add_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(this);

        popup.show();
    }

    private void doLogin() {
        ConfigStorage.loadData(this);
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("email", ConfigStorage.getEmail());
        startActivityForResult(intent, LOGIN_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case LOGIN_CODE:
                if(data == null) {
                    finish();
                    return;
                }
                SharedPreferences.Editor editor =  config.edit();
                editor.putString("email", data.getStringExtra("email"));
                if(data.getBooleanExtra("remember", false))
                    editor.putString("token", data.getStringExtra("token"));
                editor.commit();
                ConfigStorage.loadData(this);
                loadWallets();
            break;

            case EDIT_WALLET_CODE:
                if(data == null) return;
                Wallet w = wdb.findById(data.getIntExtra("id", 0))
                        .setAmount(data.getDoubleExtra("amount", 0))
                        .setName(data.getStringExtra("name"));
                wdb.update(w);
                break;
            case NEW_WALLET_CODE:
                if(data == null) return;
                wdb.add(new Wallet().setAmount(data.getDoubleExtra("amount", 0))
                        .setName(data.getStringExtra("name")));
                break;

            case DETAIL_CODE:
                if(walletAdapter != null)
                    walletAdapter.notifyDataSetChanged();
                break;
        }

    }

    private void loadWallets() {
        // carrega dados
        wdb = new WalletDB(this);
        walletAdapter = new WalletAdapter(wdb, getLayoutInflater());
        wdb.setWalletAdapter(walletAdapter);

        // carrega listView
        listView = (ListView) findViewById(R.id.main_activity_list_view);
        listView.setAdapter(walletAdapter);
        registerForContextMenu(listView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        Log.d("menu", "option");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.logout:
                config.edit().remove("token").remove("email").commit();
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_wallet:
                addOrEditWallet(null);
                return true;
            default:
                return false;
        }
    }

    public void addOrEditWallet(Wallet w) {
        Intent intent = new Intent(this, EditWalletActivity.class);
        if(w == null) {
            startActivityForResult(intent, NEW_WALLET_CODE);
        } else {
            intent.putExtra("name", w.getName());
            intent.putExtra("id", w.getId());
            intent.putExtra("amount", w.getAmount());
            startActivityForResult(intent, EDIT_WALLET_CODE);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.wallet_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.wallet_context_menu_edit:
                AdapterView.AdapterContextMenuInfo editpos = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                addOrEditWallet(wdb.get(editpos.position));
                return true;
            case R.id.wallet_context_menu_delete:
                AdapterView.AdapterContextMenuInfo deletepos = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                FragmentManager fm = getSupportFragmentManager();
                ConfirmWalletDestroyDialog cf = new ConfirmWalletDestroyDialog();
                cf.show(fm, "Confirma exclusão?");
                walletToDestroy = deletepos.position;
                return true;
            case R.id.wallet_context_menu_details:
                openDetails(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void openDetails(int position) {
        Intent intent = new Intent(this, DetailsActivity.class);
        Wallet w = wdb.get(position);
        intent.putExtra("wallet_id", w.getId());
        intent.putExtra("wallet_name", w.getName());
        intent.putExtra("wallet_amount", w.getAmount());
        startActivityForResult(intent, DETAIL_CODE);
    }

    public void onDestroyWalletClicked() {
        wdb.remove(walletToDestroy);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Intent serviceIntent = new Intent(this, ClearDBService.class);
        startService(serviceIntent);
    }
}
