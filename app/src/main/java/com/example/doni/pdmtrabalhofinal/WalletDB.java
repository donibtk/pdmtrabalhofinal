package com.example.doni.pdmtrabalhofinal;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by doni on 03/06/17.
 */

public class WalletDB {
    private String url = "https://pdm-api.herokuapp.com/wallet/";
    private String token = "aabbccddeeff";
    private Context context;
    private RequestQueue queue;
    private WalletAdapter walletAdapter;
    public final static List<Integer> removed = new ArrayList<>();

    private List<Wallet> wallets = new ArrayList<>();

    public WalletDB(Context c) {
        context = c;
        this.wallets = new ArrayList<>();
        queue = Volley.newRequestQueue(c);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url + "?token=" + token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rest: ", response);
                        try {
                            JSONArray jsonWallets = new JSONArray(response);
                            for(int i=0; i<jsonWallets.length(); i++){
                                JSONObject jwo = jsonWallets.getJSONObject(i);
                                Wallet w = new Wallet();
                                if(jwo.has("id"))
                                    w.setId(jwo.getInt("id"));
                                if(jwo.has("name"))
                                    w.setName(jwo.getString("name"));
                                if(jwo.has("amount"))
                                   w.setAmount(jwo.getDouble("amount"));
                                wallets.add(w);
                                if(walletAdapter != null)
                                    walletAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Erro Rest", Toast.LENGTH_SHORT).show();
            }
        });

        queue.add(stringRequest);
    }

    public void add(final Wallet w) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rest: ", response);
                        try {
                            JSONObject jwo = new JSONObject(response);
                            Wallet w = new Wallet();
                            if(jwo.has("id"))
                                w.setId(jwo.getInt("id"));
                            if(jwo.has("name"))
                                w.setName(jwo.getString("name"));
                            if(jwo.has("amount"))
                                w.setAmount(jwo.getDouble("amount"));
                            wallets.add(w);
                            if(walletAdapter != null)
                                walletAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Erro Rest", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject wallet = new JSONObject();
                try {
                    wallet.put("name", w.getName());
                    wallet.put("amount", w.getAmount());
                    wallet.put("token", token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("post", wallet.toString());
                return wallet.toString().getBytes();
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }
        };
        queue.add(stringRequest);

    }

    public int size() {
        return wallets.size();
    }

    public Wallet get(int p) {
        return wallets.get(p);
    }

    public void remove(int p) {
        final Wallet w = wallets.get(p);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url + w.getId() + "?token=" + token,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rest: ", response);

                        wallets.remove(findIndex(w.getId()));
                        if(walletAdapter != null)
                            walletAdapter.notifyDataSetChanged();
                        removed.add(w.getId());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Erro Rest", Toast.LENGTH_SHORT).show();
            }
        }) {
        };
        queue.add(stringRequest);
    }

    public void update(final Wallet w) {
        if(findIndex(w.getId()) == -1) return;

        StringRequest stringRequest = new StringRequest(Request.Method.PATCH, url + w.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("rest: ", response);
                        try {
                            JSONObject jwo = new JSONObject(response);
                            Wallet w = new Wallet();
                            if(jwo.has("id"))
                                w.setId(jwo.getInt("id"));
                            if(jwo.has("name"))
                                w.setName(jwo.getString("name"));
                            if(jwo.has("amount"))
                                w.setAmount(jwo.getDouble("amount"));
                            wallets.remove(findIndex(w.getId()));
                            wallets.add(w);
                            if(walletAdapter != null)
                                walletAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Erro Rest", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public byte[] getBody() throws AuthFailureError {
                JSONObject wallet = new JSONObject();
                try {
                    wallet.put("name", w.getName());
                    wallet.put("amount", w.getAmount());
                    wallet.put("token", token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("post", wallet.toString());
                return wallet.toString().getBytes();
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Content-Type","application/json");
                return headers;
            }
        };
        queue.add(stringRequest);


    }

    public Wallet findById(int id) {
        int t = size();
        for(int i = 0; i < t; i++)
            if(wallets.get(i).getId() == id)
                return wallets.get(i);

        return null;
    }

    public int findIndex(int id) {
        int t = size();
        for(int i = 0; i < t; i++)
            if(wallets.get(i).getId() == id)
                return i;

        return -1;
    }


    public void setWalletAdapter(WalletAdapter walletAdapter) {
        this.walletAdapter = walletAdapter;
    }
}
