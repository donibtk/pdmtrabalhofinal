package com.example.doni.pdmtrabalhofinal;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MovementNewOrEditFragment extends DialogFragment {

    private InsertAmountListener listener;
    private View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.movement_dialog, null);

        final EditText ed = (EditText) view.findViewById(R.id.movement_dialog_amount);
        final RadioGroup rg = (RadioGroup) view.findViewById(R.id.movement_dialog_radio);
        final EditText dt = (EditText) view.findViewById(R.id.movement_dialog_detail);

        builder.setView(view);

        builder.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Double d = null;
                try {
                    d = Double.valueOf(ed.getText().toString());
                    if(rg.getCheckedRadioButtonId() == R.id.movement_dialog_out)
                        d *= -1;
                    String detail = dt.getText().toString();
                    if(!detail.equals(""))
                        listener.onDialogPositiveClick(d, detail);
                } catch (Exception e) {}
            }
        });


        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d(getActivity().toString(), "Cancelar");
            }
        });

        Movement movement = listener.getMovement();
        if(movement != null) {
            String detail = listener.getMovement().getDescription();
            dt.setText(detail);

            Double value = listener.getMovement().getValue();
            if(value < 0) {
                value *= -1;
                RadioButton rb = (RadioButton) view.findViewById(R.id.movement_dialog_out);
                rb.setChecked(true);
            }
            ed.setText(value + "");
        }

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (InsertAmountListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement InsertAmountListener");
        }
    }

    public interface InsertAmountListener {
        void onDialogPositiveClick(Double amount, String detail);
        Movement getMovement();
    }
}
