package com.example.doni.pdmtrabalhofinal;

/**
 * Created by doni on 03/06/17.
 */

public class Wallet {
    private double amount;
    private String name;
    private int id;


    public double getAmount() {
        return amount;
    }

    public Wallet setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public String getName() {
        return name;
    }

    public Wallet setName(String name) {
        this.name = name;
        return this;
    }

    public Wallet setId(int id) {
        this.id = id;
        return this;
    }

    public int getId() {
        return id;
    }
}
