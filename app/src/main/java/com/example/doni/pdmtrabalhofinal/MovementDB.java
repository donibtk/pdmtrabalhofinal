package com.example.doni.pdmtrabalhofinal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MovementDB  extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "products";
    private static final int DATABASE_VERSION = 1;
    private MovementAdapter mAdapter;

    public MovementDB(Context context, MovementAdapter mAdapter) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mAdapter = mAdapter;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE movements (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "wallet_id INTEGER," +
                "description TEXT," +
                "value REAL)"
                );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void update(Movement m) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("description", m.getDescription());
        values.put("value", m.getValue());
        db.update("movements", values, "id = " + m.getId(), null);
        mAdapter.notifyDataSetChanged();
        Log.d("sql", m.toString());
    }

    public void add(Movement m) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("description", m.getDescription());
        values.put("value", m.getValue());
        values.put("wallet_id", m.getWalletId());
        db.insert("movements", null, values);
        mAdapter.notifyDataSetChanged();
        Log.d("sql", m.toString());
    }

    public List<Movement> getMovements(Integer walletId) {
        List<Movement> movements = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        String where = walletId == null ? "" : "WHERE wallet_id = " + walletId;

        Cursor cursor = db.rawQuery("SELECT * FROM movements " + where, null);

        if(cursor.moveToFirst()) {
            do {
                movements.add(new Movement(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getString(2),
                        cursor.getDouble(3)

                ));
            } while (cursor.moveToNext());
        }

        return movements;
    }

    public void remove(Movement m) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("movements", "id = " + m.getId(), null);
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

}
