package com.example.doni.pdmtrabalhofinal;


import android.content.SharedPreferences;

public class ConfigStorage {
    private static String token = null;
    private static String email = null;


    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ConfigStorage.token = token;
    }

    public static boolean isLoggedIn() {
        return token != null;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        ConfigStorage.email = email;
    }

    public static void loadData(MainActivity mainActivity) {
        SharedPreferences config = mainActivity.getSharedPreferences("config",
                                                            mainActivity.MODE_PRIVATE);
        token = config.getString("token", null);
        email = config.getString("email", null);
    }
}
