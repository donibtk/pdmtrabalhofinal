package com.example.doni.pdmtrabalhofinal;

public class Movement {
    private Integer id;
    private Integer walletId;
    private String description;
    private Double value;

    public Movement(Integer id, Integer walletId, String description, Double value) {
        this.setId(id);
        this.setWalletId(walletId);
        this.setDescription(description);
        this.setValue(value);
    }

    public Movement() {
        this.id = null;
    }


    public Integer getId() {
        return id;
    }

    public Movement setId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getWalletId() {
        return walletId;
    }

    public Movement setWalletId(Integer walletId) {
        this.walletId = walletId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Movement setDescription(String description) {
        this.description = description;
        return this;
    }

    public Double getValue() {
        return value;
    }

    public Movement setValue(Double value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return ("[id: " + ((id != null) ? id : "null")
                + "walletID: " + (walletId != null ? walletId : "null")
                + "description: " + (description != null ? description : "null")
                + "value: " + (value != null ? value : "null"));
    }
}
